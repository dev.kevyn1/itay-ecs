import { Event1 } from "itay-events";
import { Entity } from "./Entity";
import { ComponentClass } from "./ComponentClass";
import { EntitiesFilter } from "./EntitiesFilter";
import { EntitiesObservation as Observation, EntityAddedArgs, EntityRemovedArgs } from "./EntitiesObservation";
import { ComponentsCollection } from "./ComponentsCollection";

class MyEntityAddedArgs implements EntityAddedArgs {
	public onRemoved = new Event1<EntityRemovedArgs>();

	constructor(public entity: Entity) {
	}
}

class ComponentsCollectionObservations {
	public observationsSet = new Set<Observation>();
	public components: ComponentsCollection;

	constructor(componentKeys: IterableIterator<ComponentClass>) {
		this.components = new ComponentsCollection();
		this.components.add(Object, ...componentKeys);
	}
}

export class EntitiesCollection {
	private entitiesSet = new Set<Entity>();
	private entityAddedArgsSymbol = Symbol(EntitiesCollection.name);
	private allObservations = new Set<Observation>();
	private componentsHashToObservationsMap = new Map<number, ComponentsCollectionObservations>();

	public get size(): number {
		return this.entitiesSet.size;
	}

	public values(): IterableIterator<Entity> {
		return this.entitiesSet.values();
	}

	public has(entity: Entity): boolean {
		return this.entitiesSet.has(entity);
	}

	public add(entity: Entity): void {
		if (this.entitiesSet.has(entity)) return;

		this.entitiesSet.add(entity);

		let addedArgs = this.createEntityAddedArgs(entity);

		let observations = this.getOrCreateComponentsObservationsSet(entity.components);

		for (let observation of observations.observationsSet) {
			observation.added(addedArgs);
		}
	}

	public remove(entity: Entity): boolean {
		let removed = this.entitiesSet.delete(entity);

		if (removed) {
			let addedArgs = this.getEntityAddedArgs(entity);
			this.deleteEntityAddedArgs(entity);

			addedArgs.onRemoved.trigger({ entity: entity });
		}

		return removed;
	}

	public getByFilter(filter: EntitiesFilter): Entity[] {
		return Array.from(this.values()).filter(entity => filter.check(entity.components));
	}

	public getAddedArgsByFilter(filter: EntitiesFilter): EntityAddedArgs[] {
		return Array.from(this.values())
			.filter(entity => filter.check(entity.components))
			.map(entity => this.getEntityAddedArgs(entity));
	}

	public getByComponent(componentClass: ComponentClass): Entity[] {
		return Array.from(this.values()).filter(entity => entity.components.contains(componentClass));
	}

	public getByComponentAny(...componentClasses: ComponentClass[]): Entity[] {
		return Array.from(this.values()).filter(entity => {
			return entity.components.containsAny(componentClasses);
		});
	}

	public addObservation(observation: Observation) {
		this.allObservations.add(observation);

		for (let [hash, componentsObservations] of this.componentsHashToObservationsMap) {
			if (observation.filter.check(componentsObservations.components)) {
				componentsObservations.observationsSet.add(observation);
			}
		}
	}

	public removeObservation(observation: Observation) {
		this.allObservations.delete(observation);

		for (let [hash, componentsObservations] of this.componentsHashToObservationsMap) {
			componentsObservations.observationsSet.delete(observation);

			if (componentsObservations.observationsSet.size === 0) {
				this.componentsHashToObservationsMap.delete(hash);
			}
		}
	}

	private createEntityAddedArgs(entity: Entity): MyEntityAddedArgs {
		let args = new MyEntityAddedArgs(entity);
		(entity as any)[this.entityAddedArgsSymbol] = args;

		return args;
	}

	private getEntityAddedArgs(entity: Entity): MyEntityAddedArgs {
		return (entity as any)[this.entityAddedArgsSymbol];
	}

	private deleteEntityAddedArgs(entity: Entity): void {
		delete (entity as any)[this.entityAddedArgsSymbol];
	}

	private getComponentsObservationsSet(components: ComponentsCollection
	): ComponentsCollectionObservations | undefined {
		return this.componentsHashToObservationsMap.get(components.getHashCode());
	}

	private getOrCreateComponentsObservationsSet(components: ComponentsCollection
	): ComponentsCollectionObservations {
		let hash = components.getHashCode();
		let observations = this.componentsHashToObservationsMap.get(hash);

		if (!observations) {
			observations = new ComponentsCollectionObservations(components.keys());

			for (let observation of this.allObservations) {
				if (observation.filter.check(components)) {
					observations.observationsSet.add(observation);
				}
			}

			this.componentsHashToObservationsMap.set(hash, observations);
		}

		return observations;
	}
}