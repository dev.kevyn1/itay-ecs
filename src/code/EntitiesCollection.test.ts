import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { EntitiesCollection } from "./EntitiesCollection";
import { EntitiesObservation, EntityAddedArgs, EntityRemovedArgs } from "./EntitiesObservation";
import { ComponentsCollection } from "./ComponentsCollection";
import { Entity } from "./Entity";
import { EntitiesFilter } from "./EntitiesFilter";

class TankEntity implements Entity {
	public components: ComponentsCollection = new ComponentsCollection();

	constructor() {
		this.components.add(new GunsComponent(10));
		this.components.add(new ArmorComponent(7));
	}
}

class GunsComponent {
	public power: number = 1;

	constructor(power?: number) {
		if (power) {
			this.power = power;
		}
	}
}

class ArmorComponent {
	public durability: number = 1;

	constructor(durability?: number) {
		if (durability) {
			this.durability = durability;
		}
	}
}

export default function (suite: TestSuite): void {
	suite.describe("EntitiesCollection", suite => {
		suite.describe("add", suite => {
			suite.test("One item.", t => {
				t.arrange();
				let collection = new EntitiesCollection();
				let expected = new TankEntity();

				t.act();
				collection.add(expected);

				t.assert();
				let actual = collection.getByComponent(GunsComponent)[0];
				expect(actual).to.equal(expected);
			});

			suite.test("Multiple different items.", t => {
				t.arrange();
				let collection = new EntitiesCollection();
				let expected1 = new TankEntity();
				let expected2 = new TankEntity();

				t.act();
				collection.add(expected1);
				collection.add(expected2);

				t.assert();
				let actual = collection.getByComponent(GunsComponent);

				expect(actual.length).to.equal(2);
				expect(actual.indexOf(expected1)).to.be.greaterThan(-1);
				expect(actual.indexOf(expected2)).to.be.greaterThan(-1);
			});
		});

		suite.describe("getByComponent", suite => {

			suite.test("By component", t => {
				t.arrange();
				let collection = new EntitiesCollection();
				let item = new TankEntity();

				collection.add(item);

				t.act();
				let actual1 = collection.getByComponent(GunsComponent);
				let actual2 = collection.getByComponent(ArmorComponent);
				let empty = collection.getByComponent(Date);

				t.assert();
				expect(actual1[0]).to.equal(item);
				expect(actual2[0]).to.equal(item);
				expect(empty.length).to.equal(0);
			});
		});

		suite.describe("remove", suite => {

			suite.test("Simple", t => {
				t.arrange();
				let collection = new EntitiesCollection();
				let item = new TankEntity();

				collection.add(item);

				t.act();
				collection.remove(item);

				t.assert();
				expect(collection.size).to.equal(0);
			});
		});

		suite.describe("Observations", suite => {

			suite.test("ContainsAny filter, triggers the observation.", t => {
				t.arrange();
				let collection = new EntitiesCollection();
				let item = new TankEntity();
				let addedTriggered = false;

				t.act();

				let observation = new EntitiesObservation();
				observation.added = () => addedTriggered = true;
				observation.filter = EntitiesFilter.componentsContainsAny([GunsComponent]);

				collection.addObservation(observation);

				collection.add(item);

				t.assert();
				expect(addedTriggered).to.be.true;
			});

			suite.test("ContainsAll filter, triggers the observation.", t => {
				t.arrange();
				let collection = new EntitiesCollection();
				let item = new TankEntity();
				let addedTriggered = false;

				t.act();

				let observation = new EntitiesObservation();
				observation.added = () => addedTriggered = true;
				observation.filter = EntitiesFilter.componentsContainsAll([GunsComponent, ArmorComponent]);

				collection.addObservation(observation);

				collection.add(item);

				t.assert();
				expect(addedTriggered).to.be.true;
			});

			suite.test("When observation has ContainsAll filter, and entity match only 1 of 2 components,"
				+ " then the observation is NOT triggered.", t => {
					t.arrange();
					let collection = new EntitiesCollection();
					let item = new TankEntity();
					let addedTriggered = false;

					t.act();

					let observation = new EntitiesObservation();
					observation.added = () => addedTriggered = true;
					observation.filter = EntitiesFilter.componentsContainsAll([GunsComponent, Number]);

					collection.addObservation(observation);

					collection.add(item);

					t.assert();
					expect(addedTriggered).to.be.false;
				});

			suite.test("PassAll filter, triggers the observation.", t => {
				t.arrange();
				let collection = new EntitiesCollection();
				let item = new TankEntity();
				let addedTriggered = false;

				t.act();

				let observation = new EntitiesObservation();
				observation.added = () => addedTriggered = true;
				observation.filter = EntitiesFilter.passAll();

				collection.addObservation(observation);

				collection.add(item);

				t.assert();
				expect(addedTriggered).to.be.true;
			});

			suite.test("Adding EXISTING entity that matches, does NOT trigger the observation.", t => {
				t.arrange();
				let collection = new EntitiesCollection();
				let item = new TankEntity();
				collection.add(item);

				let observation = new EntitiesObservation();
				let addedTriggered = false;
				observation.added = () => addedTriggered = true;
				observation.filter = EntitiesFilter.componentsContainsAny([GunsComponent]);

				collection.addObservation(observation);

				t.act();
				collection.add(item);

				t.assert();
				expect(addedTriggered).to.be.false;
			});

			suite.test("Adding an entity that does NOT match, is NOT triggering the observation.", t => {
				t.arrange();
				let collection = new EntitiesCollection();
				let item = new TankEntity();
				let addedTriggered = false;

				t.act();

				let observation = new EntitiesObservation();
				observation.added = () => addedTriggered = true;
				observation.filter = EntitiesFilter.componentsContainsAny([Date]);

				collection.addObservation(observation);

				collection.add(item);

				t.assert();
				expect(addedTriggered).to.be.false;
			});

			suite.test("Removing an entity that matched, triggers the observation.", t => {
				t.arrange();
				let collection = new EntitiesCollection();
				let item = new TankEntity();

				let removedTriggered = false;

				t.act();

				let observation = new EntitiesObservation();
				observation.filter = EntitiesFilter.componentsContainsAny([GunsComponent]);
				observation.added = e => e.onRemoved.addOnce(() => removedTriggered = true);

				collection.addObservation(observation);

				collection.add(item);
				collection.remove(item);

				t.assert();
				expect(removedTriggered).to.be.true;
			});

			suite.test("Removing an entity that does NOT match, is NOT triggering the observation.", t => {
				t.arrange();
				let collection = new EntitiesCollection();
				let item = new TankEntity();

				let removedTriggered = true;

				t.act();

				let observation = new EntitiesObservation();
				observation.filter = EntitiesFilter.componentsContainsAny([Date]);
				observation.added = e => e.onRemoved.addOnce(() => removedTriggered = true);

				collection.addObservation(observation);

				collection.add(item);
				collection.remove(item);

				t.assert();
				expect(removedTriggered).to.be.true;
			});

			suite.test("Removed observation, should NOT be notified on entity added.", t => {
				t.arrange();
				let collection = new EntitiesCollection();
				let item = new TankEntity();
				let addedTriggered = false;

				t.act();

				let observation = new EntitiesObservation();
				observation.added = () => addedTriggered = true;
				observation.filter = EntitiesFilter.componentsContainsAny([GunsComponent]);

				collection.addObservation(observation);
				collection.removeObservation(observation);

				collection.add(item);

				t.assert();
				expect(addedTriggered).to.be.false;
			});

			suite.test("Observations should be called in the order of registration.", t => {
				t.arrange();
				let collection = new EntitiesCollection();
				let item = new TankEntity();
				let orderArr: number[] = [];

				t.act();

				let observation0 = new EntitiesObservation();
				observation0.added = () => orderArr.push(0);
				observation0.filter = EntitiesFilter.componentsContainsAny([GunsComponent]);

				let observation1 = new EntitiesObservation();
				observation1.added = () => orderArr.push(1);
				observation1.filter = EntitiesFilter.componentsContainsAny([ArmorComponent]);

				let observation2 = new EntitiesObservation();
				observation2.added = () => orderArr.push(2);
				observation2.filter = EntitiesFilter.componentsContainsAny([GunsComponent]);

				collection.addObservation(observation0);
				collection.addObservation(observation1);
				collection.addObservation(observation2);

				collection.add(item);

				t.assert();
				expect(orderArr.every((value, index) => value === index)).to.be.true;
			});
		});
	});
}