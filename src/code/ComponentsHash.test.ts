import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { ComponentClass } from "./ComponentClass";
import { hashComponent, hashComponents } from "./ComponentsHash";

export default function (suite: TestSuite): void {
	suite.describe("ComponentsHash", suite => {
		suite.test("One component comparison.", t => {
			t.arrange();
			let set = new Set<number>();
			let arr: ComponentClass[] = [
				Number, String, Boolean, Date, Object, Function, Promise, Array, Float32Array
			];

			t.assert();
			for (let component of arr) {
				let hash = hashComponent(component);
				expect(set.has(hash)).to.be.false;

				set.add(hash);
			}
		});

		suite.test("Multi component order should not matter.", t => {
			t.arrange();
			let set = new Set<number>();

			t.act();
			let hash1 = hashComponents([Number, String, Boolean]);
			let hash2 = hashComponents([String, Boolean, Number]);

			t.assert();
			expect(hash1).to.equal(hash2);
		});

		suite.test("Adding component hash should be valid.", t => {
			t.arrange();
			let set = new Set<number>();

			t.act();
			let hash1 = hashComponents([Number, String, Boolean]);
			let hash2 = hashComponents([Number, String]) + hashComponent(Boolean);

			t.assert();
			expect(hash1).to.equal(hash2);
		});

		suite.test("Subtracting component hash should be valid.", t => {
			t.arrange();
			let set = new Set<number>();

			t.act();
			let hash1 = hashComponents([Number, String, Boolean]) - hashComponent(Boolean);
			let hash2 = hashComponents([Number, String]);

			t.assert();
			expect(hash1).to.equal(hash2);
		});

	});
}
